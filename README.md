# Setup
If working with rez needs to rez-env pylint as well otherwise pylint doesn't work

Need to install ack for the nerdtree search -> https://beyondgrep.com/install/

# VIM

Ctrl U - scroll upwards

Ctrl D - scroll down

:%s/foo/bar/g - replace all lines

:s/foo/bar/g - replace current line

:e - opens in current buffer

`` - go back after search

:bn - go to last buffer

dt# - delete untill character

gg - begin of file

shift g - end of file

:%s/PATTERN//gn - count of `pattern` into file

% - match brackets

## nerdtree

s opens in vsplit

ms search in node

## ctrlp

ctrl v open in vsplit

# TMUX 

*PREFIX -> C-b*

PREFIX : resize-pane -D 20 (Resizes the current pane down by 20 cells)

PREFIX : resize-pane -U 20 (Resizes the current pane upward by 20 cells)

PREFIX v vertical split

PREFIX s horizontal split

PREFIX d detach from current session

# SSH

~. - Kill hanging session
